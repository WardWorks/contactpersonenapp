package be.multimedi.contactPersonenApp;

import java.util.Objects;

public class Contactpersoon {
   String naam;
   String tel;
   String straat;
   String bus;
   String gemeente;

   public Contactpersoon(String naam, String tel, String straat, String bus, String gemeente) {
      setNaam(naam);
      setTel(tel);
      setStraat(straat);
      setBus(bus);
      setGemeente(gemeente);
   }

   public String getNaam() {
      return naam;
   }

   public void setNaam(String naam) {
      if(naam == null || naam.isBlank() ) throw new IllegalArgumentException("naam verplicht");
      this.naam = naam;
   }

   public String getTel() {
      return tel;
   }

   public void setTel(String tel) {
      if(tel == null || tel.isBlank() ) throw new IllegalArgumentException("tel verplicht");
      this.tel = tel;
   }

   public String getStraat() {
      return straat;
   }

   public void setStraat(String straat) {
      if(straat == null || straat.isBlank() ) throw new IllegalArgumentException("straat verplicht");
     this.straat = straat;
   }

   public String getBus() {
      return bus;
   }

   public void setBus(String bus) {
      if(bus == null || bus.isBlank()  ) throw new IllegalArgumentException("bus verplicht");
      this.bus = bus;
   }

   public String getGemeente() {
      return gemeente;
   }

   public void setGemeente(String gemeente) {
      if(gemeente == null || gemeente.isBlank()  ) throw new IllegalArgumentException("gemeente verplicht");
      this.gemeente = gemeente;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Contactpersoon that = (Contactpersoon) o;
      return Objects.equals(naam, that.naam) &&
              Objects.equals(tel, that.tel) &&
              Objects.equals(straat, that.straat) &&
              Objects.equals(bus, that.bus) &&
              Objects.equals(gemeente, that.gemeente);
   }

   @Override
   public int hashCode() {
      return Objects.hash(naam, tel, straat, bus, gemeente);
   }

   @Override
   public String toString() {
      return "Contactpersoon{" +
              "naam='" + naam + '\'' +
              ", tel='" + tel + '\'' +
              ", straat='" + straat + '\'' +
              ", bus='" + bus + '\'' +
              ", gemeente='" + gemeente + '\'' +
              '}';
   }
}
