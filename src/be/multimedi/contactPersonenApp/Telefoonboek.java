package be.multimedi.contactPersonenApp;

import java.util.Formatter;
import java.util.concurrent.CountDownLatch;

public class Telefoonboek {
   public static final int LIST_MAX = 100;
   Contactpersoon[] lijst = new Contactpersoon[LIST_MAX];
   int counter = 0;

   public boolean toevoegen(Contactpersoon cp) {
      //validatie
      if (cp == null)
         return false;

      for (int i=0; i<counter; i++) {
         Contactpersoon c = lijst[i];
         if (c.equals(cp)) {
            return false;
         }
      }

      if (counter >= LIST_MAX) {
         return false;
      }

      lijst[counter++] = cp;
      return true;
   }

   public boolean verwijder(Contactpersoon cp) {
      if (cp == null) return false;
      if (counter == 0) return false;
      for (int i = 0; i < counter; i++) {
         if (lijst[i].equals(cp)) {
            if (i == counter - 1) {
               lijst[i] = null;
            } else {
               for (int j = i; j < counter - 1; j++) {
                  lijst[j] = lijst[j + 1];
               }
               lijst[counter - 1] = null;

            }
            counter--;
            return true;
         }
      }
      return false;
   }

   public Contactpersoon zoekContactpersoon(String naam) {
      naam = naam.toLowerCase();
      for (int i = 0; i < counter; i++) {
         if (lijst[i].getNaam().toLowerCase().contains(naam))
            return lijst[i];
      }
      return null;
   }

   @Override
   public java.lang.String toString() {
      if (counter == 0) return "Telefoonboek is leeg!";
      Formatter f = new Formatter();
      f.format("Telefoonboek:\n");
      for (int i = 0; i < counter; i++) {
         f.format("%d. naam=%s tel=%s ...%n", i + 1, lijst[i].getNaam(), lijst[i].getTel());
      }
      return f.toString();
   }
}
